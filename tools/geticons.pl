#!/usr/bin/perl
#
# The allicons file was created by doing this in the share/icons/hicolor dir:
#   find -name '*.svg' -o -name '*.png' > ~/allicons
#
# Note that these won't include the stock GTK+ icons, which need to be added
# separately (using getstockitems.pl).
#

$IconsFile = "allicons";
$Output = "icons.inc";

open(INPUT, $IconsFile) || die "Can't open input";
open(OUTPUT, ">$Output") || die "Can't open output";

# Read in the list of icons and create a hash table, using the pathname of
# the icons without the leading sizes or .png/.svg suffixes, so we end up
# with a hash of all the unique icon names.
while(<INPUT>) {
    if (m%^\./(\d+x\d+|scalable)/(.*)\.(png|svg)$%) {
	$NamedIcons{$2} = $2;
    } else {
	die "Couldn't match line";
    }
}
close(INPUT);

# Sort the icons and output them, stripping off the leading directory names.
foreach $icon (sort (keys (%NamedIcons))) {
    $icon =~ m%^(.*/)(.*)%;
    $dir = $1;
    $basename = $2;
    if ($dir !~ m/^apps/) {
#	print OUTPUT "$dir$basename\n";
	print OUTPUT "  \"$basename\",\n";
    }
}


close(OUTPUT);
