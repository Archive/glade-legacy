#!/usr/bin/perl
#
# Writes out a template for a new GbWidget into the current directory.
# It promps the user for a few pieces of information about the new widget:
#
# 1. The widget's class name.
# 2. The tooltip to show on the palette (a very short description).
# 3. The name of the pixmap file (just the last part). The pixmap should be in
#       the glade/graphics directory.
# 4. The C structure in the pixmap file (since it is #include'd in the source.)
# 5. The function prefix used in the widget, e.g. 'gtk_button_'. This is
#       turned into a function prefix in the output file.
#

$class = &GetInput ("Widget Class Name (e.g. GtkButton): ");
$tooltip = &GetInput ("Tooltip for palette (e.g. 'Button'): ");
$icon_file = &GetInput ("Icon pixmap filename (e.g. button.xpm): ");
$icon = &GetInput ("Icon structure name (from xpm file): ");
$gtkfunc = &GetInput ("Widget function prefix (e.g. 'gtk_button_'): ");

&OutputWidgetFile;

# add to Makefile bits
print "Now add ${file}.c to Makefile.am and po/POTFILES.in\n";
print "and to one of the palette files, e.g. glade_gtk12lib.c\n";

1;


sub GetInput {
    print $_[0];
    $input = <>;
    $input =~ s/\n*\r*//g;
    return $input;
}


sub OutputWidgetFile {
    $file = "\L$class";
    $gtkfunc =~ s/_$//;
    $func = $gtkfunc;
    $file =~ s/^gtk/gb/;
    $func =~ s/^gtk/gb/;
    $func =~ s/^gnome/gb_gnome/;

    print "Creating File: ${file}.c\n";

    open(OUTPUT, ">$file.c") || die "Can't open output";
    print(OUTPUT <<EOF);
/*  Gtk+ User Interface Builder
 *  Copyright (C) 1999-2002  Damon Chaplin
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include <gtk/gtk.h>
#include "../gb.h"

EOF

    if ($icon_file) { print(OUTPUT <<EOF); }
/* Include the 21x21 icon pixmap for this widget, to be used in the palette */
#include "../graphics/$icon_file"

EOF

    else { print(OUTPUT <<EOF); }
/* Include the 21x21 icon pixmap for this widget, to be used in the palette */
/* This widget has no icon yet. */
/*#include "../graphics/"*/

EOF


    print(OUTPUT <<EOF);
/*
 * This is the GbWidget struct for this widget (see ../gbwidget.h).
 * It is initialized in the init() function at the end of this file
 */
static GbWidget gbwidget;



/******
 * NOTE: To use these functions you need to uncomment them AND add a pointer
 * to the function in the GbWidget struct at the end of this file.
 ******/

/*
 * Creates a new GtkWidget of class ${class}, performing any specialized
 * initialization needed for the widget to work correctly in this environment.
 * If a dialog box is used to initialize the widget, return NULL from this
 * function, and call data->callback with your new widget when it is done.
 */
/*
static GtkWidget*
${func}_new (GbWidgetNewData *data)
{

}
*/



/*
 * Creates the components needed to edit the extra properties of this widget.
 */
/*
static void
${func}_create_properties (GtkWidget * widget, GbWidgetCreateArgData * data)
{

}
*/



/*
 * Gets the properties of the widget. This is used for both displaying the
 * properties in the property editor, and also for saving the properties.
 */
/*
static void
${func}_get_properties (GtkWidget *widget, GbWidgetGetArgData * data)
{

}
*/



/*
 * Sets the properties of the widget. This is used for both applying the
 * properties changed in the property editor, and also for loading.
 */
/*
static void
${func}_set_properties (GtkWidget * widget, GbWidgetSetArgData * data)
{

}
*/



/*
 * Adds menu items to a context menu which is just about to appear!
 * Add commands to aid in editing a ${class}, with signals pointing to
 * other functions in this file.
 */
/*
static void
${func}_create_popup_menu (GtkWidget * widget, GbWidgetCreateMenuData * data)
{

}
*/



/*
 * Writes the source code needed to create this widget.
 * You have to output everything necessary to create the widget here, though
 * there are some convenience functions to help.
 */
/*
static void
${func}_write_source (GtkWidget * widget, GbWidgetWriteSourceData * data)
{

}
*/



/*
 * Initializes the GbWidget structure.
 * I've placed this at the end of the file so we don't have to include
 * declarations of all the functions.
 */
GbWidget*
${func}_init ()
{
  /* Initialise the GTK type */
  volatile GtkType type;
  type = ${gtkfunc}_get_type();

  /* Initialize the GbWidget structure */
  gb_widget_init_struct(&gbwidget);

  /* Fill in the pixmap struct & tooltip */
EOF

    if ($icon_file) { print(OUTPUT <<EOF); }
  gbwidget.pixmap_struct = $icon;
EOF
    else { print(OUTPUT <<EOF); }
  gbwidget.pixmap_struct = NULL;
EOF

    if ($tooltip) { print(OUTPUT <<EOF); }
  gbwidget.tooltip = _("$tooltip");
EOF
    else { print(OUTPUT <<EOF); }
  gbwidget.tooltip = NULL;
EOF

    print(OUTPUT <<EOF);

  /* Fill in any functions that this GbWidget has */
/*
  gbwidget.gb_widget_new		= ${func}_new;
  gbwidget.gb_widget_create_properties	= ${func}_create_properties;
  gbwidget.gb_widget_get_properties	= ${func}_get_properties;
  gbwidget.gb_widget_set_properties	= ${func}_set_properties;
  gbwidget.gb_widget_create_popup_menu	= ${func}_create_popup_menu;
  gbwidget.gb_widget_write_source	= ${func}_write_source;
*/

  return &gbwidget;
}

EOF
    close(OUTPUT);
}
