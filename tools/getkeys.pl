#!/usr/bin/perl

$KeysFile = "/usr/include/gtk-2.0/gdk/gdkkeysyms.h";
$Output = "keys.inc";

open(KEYS, $KeysFile) || die "Can't open input";
open(OUTPUT, ">$Output") || die "Can't open output";

print(OUTPUT <<EOF);
const GbKey GbKeys[] =
{
EOF

# We collect common keys in sections so we output them first.
$sect1 = $sect2 = $sect3 = $sect4 = $sect5 = $sect6 = $rest = "";

while(<KEYS>) {
    if (m/^#define GDK_(\S+)\s+(\S+)/) {
	$key = $1;
	$val = $2;

#	    print "Found key: $key, val: $val\n";
	$len = length ($key);
	if ($len < 29) {
	    $pad = ' ' x (29 - length ($key));
	} else {
	    $pad = "";
	}

	$output = "  { GDK_$key, $pad\"$key\" },\n";


	if ($key =~ m/^[a-zA-Z]$/) {
	    $sect1 .= $output;
	} elsif ($key =~ m/^F\d+$/) {
	    $sect2 .= $output;
	} elsif ($key eq 'BackSpace'
		 || $key eq 'space'
		 || $key eq 'Tab'
		 || $key eq 'Return'
		 || $key eq 'Escape'
		 || $key eq 'Delete'
		 || $key eq 'Insert') {
	    $sect3 .= $output;
	} elsif ($key eq 'Home'
		 || $key eq 'End'
		 || $key eq 'Page_Up'
		 || $key eq 'Page_Down'
		 || $key eq 'Up'
		 || $key eq 'Down'
		 || $key eq 'Left'
		 || $key eq 'Right') {
	    $sect4 .= $output;
	} elsif ($key =~ m/^KP_/) {
	    $sect5 .= $output;
	} elsif ($key =~ m/^\d$/) {
	    $sect6 .= $output;
	} else {
	    $rest .= $output;
	}
    } else {
#	print "Not a key: $_\n";
    }
}
close(KEYS);

    print(OUTPUT <<EOF);
$sect1
$sect2
$sect3
$sect4
$sect5
$sect6
$rest
  { 0, NULL }
};
EOF

close(OUTPUT);
