#!/usr/bin/perl
#
# This gets all the GTK+ stock items from gtkstock.h and outputs them
# ready to be added to named_icons.c
#

$Stock_H_File = "/opt/gnome/cvs/gtk+/gtk/gtkstock.h";
$Output = "stock.inc";

open(INPUT, $Stock_H_File) || die "Can't open input";
open(OUTPUT, ">$Output") || die "Can't open output";

while(<INPUT>) {
    if (m/^\#define (GTK_STOCK_\S+)/) {
	print (OUTPUT "  $1,\n");
    }
}
close(INPUT);
close(OUTPUT);
